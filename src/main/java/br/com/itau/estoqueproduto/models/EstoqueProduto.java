package br.com.itau.estoqueproduto.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

//** A anotação @Entity é utilizada para informar que uma classe também é uma entidade.                                     **
//** A partir disso, a JPA estabelecerá a ligação entre a entidade e uma tabela de mesmo nome no banco de dados             **
//** A anotação @Table é utilizada para refenrenciar o nome da tabela que estaremos utilizando

@Entity
@Table(name="estoqueproduto")
public class EstoqueProduto {

	//** A anotação @Id serve para identificar o campo chave da sua entidade.
	@Id
	private int idEstoqueProduto;
	
	@NotNull
	private int quantidadeProdutos;

	public int getIdProduto() {
		return idEstoqueProduto;
	}

	public void setIdProduto(int idProduto) {
		this.idEstoqueProduto = idProduto;
	}

	public int getQuantidadeProdutos() {
		return quantidadeProdutos;
	}

	public void setQuantidadeProdutos(int quantidadeProdutos) {
		this.quantidadeProdutos = quantidadeProdutos;
	}
	
	
}
