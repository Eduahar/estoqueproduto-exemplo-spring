package br.com.itau.estoqueproduto.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.estoqueproduto.models.EstoqueProduto;
import br.com.itau.estoqueproduto.services.EstoqueProdutoService;


//** A anotação @RestController nos permite criar um controlador com características REST e que possa manipular as requisições 
@RestController

//** A anotação @RequestMapping serve para colocar o endereço da sua aplicação, um exemplo de como ficaria esta chamada
//**  ex http://localhost:8888/estoqueproduto
@RequestMapping("/estoqueproduto")
public class EstoqueProdutoController {

	//** a grosso modo a anotação @Autowired funciona como um instanciação unica "por baixo dos panos"	
	@Autowired
	EstoqueProdutoService estoqueProdutoService;

	//** a anotação @GetMapping irá executar a requisição sem receber nenhum parametro	
	@GetMapping
	public  Iterable<EstoqueProduto> listarEstoqueProduto() {
		return estoqueProdutoService.listarEstoquesProduto();
	}

	//** a anotação @GetMapping("/{id}) irá executar a requisição recebendo um paramtro na url, ex de chamada: http://localhost:8888/estoqueproduto/1 
	@GetMapping("/{id}")

	//** a anotação @PathVariable é utilizado para o sistema entender que deveremos receber um parametro da url 
	public ResponseEntity consultarProdutoPorId(@PathVariable int id) {
		
		//** quando realizamos uma consulta especifica por um id via o CRUDRepository ele nos retorna com o Tipo OPTIONAL que nos auxilia para verificarmos se 
		//** esta consulta retornou conteudo ou não evitando nullpointers
		Optional<EstoqueProduto> optionalEstoqueProduto = estoqueProdutoService.consultarEstoquePorId(id);
		
		// validacao para verificar se a consulta que fizemos por id devolveu algum resultado
		if (optionalEstoqueProduto.isPresent()) {

			// o ResponseEntity trabalha com as respostas das requisições http que fazemos da nossa API, basicamente eu posso controlar qual
			// qual a reposta quero devolver e enviar informações no formato json como resposta
			
			// referente ao comando optionalEstoqueProduto.get(), utilizando o get() para pegar a informação buscada na base de dados dentro do objeto Optional
			return ResponseEntity.ok(optionalEstoqueProduto.get());
		}

		// Enviamos a reposta de requisição Http como notFound;
		return ResponseEntity.notFound().build();
	}
	
	//** a anotação @PostMapping irá executar a requisição recebendo informações JSON
	//** como realizar a chamada deste end-point : http://localhost:8888/estoqueproduto com a função de POST
	@PostMapping
	//** a anotação @RequestBody basicamente é utilizado para o sistema receber as informações de entrada JSON na chamada da nossa aplicação
	public void inserirEstoqueProduto(@RequestBody EstoqueProduto estoqueProduto) {
		estoqueProdutoService.inserirEstoqueProduto(estoqueProduto);
	}
}
