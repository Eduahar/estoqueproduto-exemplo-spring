package br.com.itau.estoqueproduto.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.estoqueproduto.models.EstoqueProduto;
import br.com.itau.estoqueproduto.repositories.EstoqueProdutoRepository;

//** Anotação @Service serve para definir uma classe como pertencente à camada de Serviço da aplicação
//** Basicamente no service devemos inserir a lógica do processamento do nosso programa

@Service
public class EstoqueProdutoService {

	//** a grosso modo a anotação @Autowired funciona como um instanciação unica "por baixo dos panos"
	@Autowired
	EstoqueProdutoRepository estoqueProdutoRepository;
	
	public Optional<EstoqueProduto> consultarEstoquePorId(int id) {
	
//**  realizando a chamada de um metodo do EstoqueProdutoRepository sem precisar realizar uma instanciação
//**  devido a anotação @AutoWired
//**  este metodo findById irá na tabela recuperar o registro pelo id unico, ex. Select * from estoqueproduto where id = id 
		return estoqueProdutoRepository.findById(id);			
	}
	
	public Iterable<EstoqueProduto> listarEstoquesProduto(){

//**  o metodo findAll irá listar todos os registros da tabela;
		return estoqueProdutoRepository.findAll();
	}
	
	public void inserirEstoqueProduto(EstoqueProduto estoqueProduto) {
		
//**   o metodo save irá realizar o insert do registro na tabela;		
		estoqueProdutoRepository.save(estoqueProduto);		
	}
	
}
