package br.com.itau.estoqueproduto.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.estoqueproduto.models.EstoqueProduto;


//**  O CRUDRepository é uma interface do Spring Data para operações de CRUD, 
//** ele disponibiliza alguns metódos padrões de consulta, inclusao, atualização, iremos ver a chamada desses metodos na
//** na classe EstoqueProdutoService

//** Especificação de como preencher os parametros do CRUDRepository<Entidade Criada "EstoqueProduto", O tipo da chave "@ID" da Entidade>

public interface EstoqueProdutoRepository extends CrudRepository<EstoqueProduto, Integer> {

}
